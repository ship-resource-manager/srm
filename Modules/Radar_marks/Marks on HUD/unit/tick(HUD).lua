--code is EXTREMELY unoptimized just for better understanding. Once w� get proofs of its work, w� will re-write it in a right way.

entries = radar.getEntries();
markersMap = ''; --A "layer" of marks we draw on the screen

for entryID, content in ipairs(entries) do
  local Pos = radar.getConstructPos(entryID);
  local x1 = Pos[1];
  local y1 = Pos[2];
  local z1 = Pos[3];
  local range = math.sqrt((x1 + relativeXYZ[1])*(x1 + relativeXYZ[1]) + (y1 + relativeXYZ[2])*(y1 + relativeXYZ[2]) + (z1 + relativeXYZ[3])*(z1 + relativeXYZ[3])); --we are counting from (0, 0, 0) point so the coordinates of detected thing are coordinates of vector from us to them. It's simple to count range because that.

  local normalVector = {0, y1 + relativeXYZ[2], 0}; -- Vector that is going "deeper" into screen: n(A, B, C).
  local planeEquotion = {normalVector[1], nil, normalVector[2], nil, normalVector[3], nil, 0, 0}; -- A*X + B*Y + C*Z + D = 0. There is zero D. In fact, our formula is B*Y = 0.
  local lineEquotion = {nil, x1, normalVector[1], nil, y1, normalVector[2], nil, z1, normalVector[3]}; -- (X-x1)/A = (Y-y1)/B = (Z-z1)/C.
  local equotionsSystem = {}; --needed for matrix
  equotionsSystem[1] = {normalVector[2], nil, normalVector[1], nil, normalVector[2]*x1 - normalVector[1]*y1}; -- B*X - A*Y = B*x1 - A*y1.
  equotionsSystem[2] = {normalVector[3], nil, normalVector[2], nil, normalVector[3]*y1 - normalVector[2]*z1}; -- C*Y - B*Z = C*y1 - B*z1.
  equotionsSystem[3] = {normalVector[1], nil, normalVector[2], nil, normalVector[3], nil, 0}; -- A*X + B*Y + C*Z = -D.
  
  local matrix = {}; --we'll solve it by Kramer's method
  matrix[1] = {equotionsSystem[1][1], equotionsSystem[1][3], 0};
  matrix[2] = {0, equotionsSystem[2][1], equotionsSystem[2][3]};
  matrix[3] = {equotionsSystem[3][1], equotionsSystem[3][3], equotionsSystem[3][5]};
  local freeNumbers = {equotionsSystem[1][5], equotionsSystem[2][5], equotionsSystem[3][7]};

  --solving matrix
  local mainDiagonal = matrix[1][1]*matrix[2][2]*matrix[3][3] + matrix[1][2]*matrix[2][3]*matrix[3][1] + matrix[2][1]*matrix[3][2]*matrix[1][3];
  local incidentalDiagonal = matrix[1][3]*matrix[2][2]*matrix[3][1] + matrix[1][2]*matrix[2][1]*matrix[3][1] + matrix[3][2]*matrix[2][3]*matrix[1][1];
  local determinant = mainDiagonal - incidentalDiagonal;

  local mainDiagonalX = freeNumbers[1]*matrix[2][2]*matrix[3][3] + matrix[1][2]*matrix[2][3]*freeNumbers[3] + freeNumbers[2]*matrix[3][2]*matrix[1][3];
  local incidentalDiagonalX = matrix[1][3]*matrix[2][2]*freeNumbers[3] + matrix[1][2]*freeNumbers[2]*freeNumbers[3] + matrix[3][2]*matrix[2][3]*freeNumbers[1];
  local determinantX = mainDiagonalX - incidentalDiagonalX;

  local mainDiagonalZ = matrix[1][1]*matrix[2][2]*freeNumbers[3] + matrix[1][2]*freeNumbers[2]*matrix[3][1] + matrix[2][1]*matrix[3][2]*freeNumbers[1];
  local incidentalDiagonalZ = freeNumbers[1]*matrix[2][2]*matrix[3][1] + matrix[1][2]*matrix[2][1]*matrix[3][1] + matrix[3][2]*freeNumbers[2]*matrix[1][1];
  local determinantZ = mainDiagonalZ - incidentalDiagonalZ;

  local xOnScreen = determinantX / determinant;
  local zOnScreen = determinantZ / determinant;
  --end of solving

  if (zOnScreen > screenHeight / 2) then
    zOnScreen = screenHeight / 2 - 20;
  end
  if (zOnScreen < screenHeight / -2) then
    zOnScreen = screenHeight / -2 + 20;
  end
  if (xOnScreen > screenWidth / 2) then
    xOnScreen = screenWidth / 2 - 20;
  end
  if (xOnScreen < screenWidth / -2) then
    xOnScreen = screenWidth / -2 + 20;
  end
  
  markersMap = markersMap..'<div class="div_'..entryID..'"></div><style>.div_'..entryID..'{left: '.. xOnScreen + screenWidth / 2 ..'; bottom: '.. zOnScreen + screenHeight / 2 ..'; border: 5px dashed red; border-radius: 2px; width: 40px; height: 40px;}</style>';
end

system.setScreen(markersMap..'<style>.screen_widget_wrapper {animation: none;background: none;}.screen_widget_wrapper .widget_container {border: none;}.screen_widget_wrapper .widget_header {display: none;}div {position: fixed;display: block;margin: 0;padding: 0;box-sizing: border-box;z-index: 1;}</style>');