json = require("dkjson") --place it on the 1st string, before any modules etc.

SRMCore = {} --creating class

function SRMCore:new() --constructor
  unit.hide()

  local private = {} --defining private variables / functions
    private.modules = {renderOnScreen = {}, renderOnPlayerScreen = {}, networking = {}, all = {}}
    private.ENC_private = 1
    private.ENC_public = 1
    private.notificationsList = {}
    private.shutdownKey = 1 --CHANGE THIS
    private.allies = {} --{'ME', 'MY_FRIEND', 'etc.'}
    private.enemies = {} --{'MY_WORST_ENEMY', 'MY_SECOND_ENEMY', 'etc.'}
    private.commander = '' --do not write anything here, it will be replaced anyway
    private.user = system.getPlayerName(unit.getMasterPlayerId())
    private.boardType = 'main' --'main' or 'additional'. 

    function private:encode(message, key) --has been invented by Nikon
      local encoded_msg = ''

      for i = 1, string.len(message) do
        encoded_msg = encoded_msg..tostring(string.len(tostring(string.byte(string.sub(message, i, i)) + key)) - 2)
        encoded_msg = encoded_msg..tostring(math.floor(string.byte(string.sub(message, i, i)) + key))
      end

      return encoded_msg
    end

    function private:decode(encoded_msg) --has been invented by Nikon
      local private_key = private.ENC_private
      local message = ''

      i = 1
      while i < string.len(encoded_msg) do
        local length = math.floor(tonumber(string.sub(encoded_msg, i, i)))
        local number = ''

        for j =1, length do
          number = number..(string.sub(encoded_msg, j+i, j+i))
        end

        message = message..(string.char(tonumber(number) - private_key))

        i = i + length + 1
      end

      return message
    end


  local public = {} --defining public variables / functions
	function public:sendToDatabank(key, message)
      local msgtype = type(message)

      if msgtype == 'string' then
        databank.setStringValue(key, message)
      elseif msgtype == 'number' then
        databank.setFloatValue(key, message)
      end
    end

    function public:retrieveFromDatabank(key)
      if databank.hasKey(key) == 1 then
        local status, value = pcall(databank.getStringValue, key)
      
        if status then
          return value
        else
          status, value = pcall(databank.getFloatValue, key)
        
          if status then
            return value
          else
            return databank.getIntValue(key)
          end
        end
	  else
	    return 'SRM:NO_KEY'
      end
    end

    function public:getAllies()
      return private.allies
    end

    function public:getEnemies()
      return private.enemies
    end

    function public:getCommander()
      return private.commander
    end

    function public:getBoardType()
      return private.boardType
    end

    function public:sendToModule(sender, receiverModule, message)
      receiverModule:receiveFromCore(sender, message)
    end

    function public:sendToChannel(channel, message, key) --message HAS TO be json-encoded: {"sender": ["playerNickname", "moduleName"], "receiver": ["playerNickname", "moduleName"], "body": <whatever you want> }
      local encoded_msg = private:encode(message, key)

      emitter.send(channel, encoded_msg)
    end

    function public:receiveFromChannel(channel, msg) --this function calls by receiver when it has incoming message
      local status, obj = pcall(json.decode, encoded_msg)
      
      if obj == nil then
        obj = ''
      end

      if obj.receiver ~= nil then
        if obj.receiver[1] == self:retrieveFromDatabank('commander') then
          if obj.body == 'GET_PUBLIC_KEY' then
            local response = '{"sender": ["' .. self:retrieveFromDatabank('commander') .. '", "CORE"], "receiver": ["' .. obj.sender[1] .. '", "' .. obj.sender[2] .. '"], "body": ["GET_PUBLIC_KEY_RESPONSE", ' .. private.ENC_public .. ']}'
            emitter.send(channel, response)
          elseif obj.body[1] ~= nil then
            if obj.body[1] == 'GET_PUBLIC_KEY_RESPONSE' then
              for mod in pairs(private.modules.networking) do
                if mod.networkName == obj.receiver[2] then
                  self:sendToModule('NETWORK:' .. obj.sender[1] .. ':' .. obj.sender[2], mod, obj.body)
                  break
                end
              end
            end
          end
        end
      else
        local status, message = pcall(private.decode, encoded_msg)
        status, obj = pcall(json.decode, message)
		
        if obj == nil then
          obj = ''      
        end
            
        if obj.receiver ~= nil then
          if obj.receiver[1] == self:retrieveFromDatabank('commander') then
            for mod in pairs(private.modules.networking) do
              if mod.networkName == obj.receiver[2] then
                self:sendToModule('NETWORK:' .. obj.sender[1] .. ':' .. obj.sender[2], mod, obj.body)
                break
              end
            end
          end
        end
      end
    end

    function public:getModulesList()
      return private.modules.all
    end

    function public:shutdown(key)
      if key == private.shutdownKey then
        if private.boardType == 'main' then
          databank.clear()
        end
      end
    end

    --LOADING MODULES
    for _, value in pairs(_G) do -- look for modules' configs in GLOBALS
      if type(value) == "table" then
        if value.type == "SRM_MODULE_CFG" and value.enabled == true then -- check if it is a SRM module and it is enabled
          private.modules.all[value.name] = value.object -- add module in global list

          if value.renderOnScreen == true then -- add module in list of screen users
            private.modules.renderOnScreen[value.name] = value.object
          end

          if value.renderOnPlayerScreen == true then -- add module in list of player's screen users
            private.modules.renderOnPlayerScreen[value.name] = value.object
          end

          if value.networking == true then -- add module in list of emitter&receiver users
            private.modules.networking[value.name] = value.object
          end
        end
      end
    end
    --END OF LOADING

    --SECURITY KEYS GENERATION
    local a = 0
    while a == 0 do
      a = math.random(0, 1)
    end
    local g = math.random(1000, 9999)
    local p = math.random(1000000, 9999999)
    private.ENC_public = (g^a)%p
    private.ENC_private = (private.ENC_public^a)%p
    --END OF SECURITY KEYS GENERATION

    --SETTING COMMANDER
    if private.boardType == 'main' then
      private.commander = system.getPlayerName(unit.getMasterPlayerId())
    end
    --END OF SETTING

  setmetatable(public, self)
  self.__index = self 
  return public
end

SRM = SRMCore:new()

--ADDING ALLIES / ENEMIES TO DATABANK
if databank ~= nil and SRM:getBoardType() == 'main' then
  local allies = SRM:getAllies()
  local enemies = SRM:getEnemies()
  local commander = SRM:getCommander()

  SRM:sendToDatabank('commander', commander)

  for i = 1, #allies do
    SRM:sendToDatabank(allies[i], 'ally')
  end

  for i = 1, #enemies do
    SRM:sendToDatabank(enemies[i], 'enemy')
  end
end
--END OF ADDING