Any global variables used in the core and basic module functions, but created OUTSIDE of them, will be mentioned here. IMPORTANT: don't try to use their functions directly if you don't want to, for example, lose your encryption!
To get more information about every variable read in-game codex.

emitter - an emitter unit.
receiver - a reciever unit.
screen - a screen unit.
system - player's screen (HUD).
databank - a databank.