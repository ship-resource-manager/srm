AGGController = {}

function AGGController:new()
  local private = {}
    private.SRM_config = {
      name = "AGGController",
      networkName = "AGGController",
      displayName = "AGG",
      enabled = true,
      renderOnScreen = true,
      useAdditionalScreen = false,
      renderOnPlayerScreen = true,
      displayInMainWidget = false,
      widgetCoords = {85, 30},
      widgetSize = {10, 20},
      placeValuesOnHUD = false,
      networking = false,
      useStartupHook = true,
      useShutdownHook = false
    }
    private.AGGVar = antigrav
    private.AGGTargetAltitudeStep = 500
    private.AGGTargetAltitude = 1000


  local public = {}
    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:receiveFromCore(senderNickname, senderModule, message) end

    function public:getScreenHTML()
      return 'HTML_code'
    end

    function public:getPlayerScreenHTML()
      return '<div class="' .. private.SRM_config.name .. '" style="position:fixed;left:' .. private.SRM_config.widgetCoords[1] .. ' vw;top:' .. private.SRM_config.widgetCoords[2] .. ' vh;width:' .. private.SRM_config.widgetSize[1] .. ' vw;height:' .. private.SRM_config.widgetSize[2] .. [[ vh">
      
      </div>]]
    end

    function public:onScreenClick(x, y)
      
    end

    function public:onPlayerScreenClick(x, y)
      
    end

    function public:runStartupHook()
      local query = SRM:retrieveFromDatabank('StorageDatabank', 'AGGController:aggTargetAltitude')

      if query ~= 'SRM:NO_DATABANK' then
        if query ~= 'SRM:NO_KEY' then
          private.AGGTargetAltitude = tonumber(query)
          private.AGGVar.setBaseAltitude(private.AGGTargetAltitude)

          system.print('AGG Controller: AGG target altitude set: ' .. private.AGGTargetAltitude .. 'm')
        else
          system.print('AGG Controller: can\'t retrieve AGG target altitude from the databank. It will be 1000m instead of the saved value. It\'s okay if you see that message when the script was launched for the first time after setup.')
          SRM:makeNotification('AGG Controller', 'Can\'t retrieve AGG target altitude from the databank. It will be 1000m instead of the saved value. It\'s okay if you see that message when the script was launched for the first time after setup.')
        end

      else
        system.print('AGG Controller: No databank detected. AGG target altitude can\'t be loaded and saved.')
        SRM:makeNotification('AGG Controller', 'No databank detected. AGG target altitude can\'t be loaded and saved.')
      end

      system.print('AGG Controller: Ready.')
    end

    function public:saveAGGTargetAltitude()
      SRM:sendToDatabank('StorageDatabank', 'AGGController:aggTargetAltitude', private.AGGTargetAltitude)

      system.print('AGG Controller: AGG target altitude saved as ' .. private.AGGTargetAltitude .. 'm.')
    end

    function public:decreaseAGGTargetAltitude()
      if private.AGGTargetAltitude > 1499 then
        private.AGGTargetAltitude = private.AGGTargetAltitude - private.AGGTargetAltitudeStep
        private.AGGVar.setBaseAltitude(private.AGGTargetAltitude)
        
        system.print('AGG Controller: AGG target altitude decreased. New altitude: ' .. private.AGGTargetAltitude .. 'm')
      else
        system.print('AGG Controller: AGG target altitude can\'t be less than 1000m')
      end
    end

    function public:increaseAGGTargetAltitude()
      private.AGGTargetAltitude = private.AGGTargetAltitude + private.AGGTargetAltitudeStep
      private.AGGVar.setBaseAltitude(private.AGGTargetAltitude)
        
      system.print('AGG Controller: AGG target altitude increased. New altitude: ' .. private.AGGTargetAltitude .. 'm')
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

AGGController_obj = AGGController:new()