for id, timer in pairs(SRM.awaitTimers) do
    if (timer.timeToCall >= SRM.secondsSinceStart) then
        timer.callback()
        SRM.awaitTimers[id] = nil
    end
end

SRM.secondsSinceStart = SRM.secondsSinceStart + 0.1

local notificationsList = SRM:getNotificationsList()

if notificationsList[1] ~= nil then
  if system.getTime() >= notificationsList[1].timestamp then
    SRM:removeNotification(2131242314)
  end
end