screen.activate();
system.showScreen(1);

--CHANGE IT TO YOUR VALUES
screenWidth = 1920; --Game window width in pixels
screenHeight = 1080; --Game window height in pixels
eyeLine = 1.65; -- The distance from feet to eyes in meters (if you are seating, it WON'T BE THE SAME as if you are staying). You can forget about this parameter if you don't need 100% accuracy.
relativeXYZ = {-7, -13.5, -10.5 + eyeLine}; --local seat coordinates minus local radar coordinates (X, Y, Z). Radar MUST be directioned to the same side as seat.

unit.setTimer('HUD', 1); --Timer for updating HUD every second.