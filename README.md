# Ship Resource Manager
- Developed by MopnexAndreyno and Lavayar
- Documentation: in "Documentation" folder

SRM is an open-source ship operational system for Dual Universe, which can be set up in every dynamic construct (or, if you want, in static or space construct). Let's go deeper into it.

# What is SRM
  Sometimes when you are building a ship, you have to write scripts for it. And for sure you will meet many troubles while trying to make your scripts work with each other properly. SRM solves this instead of you - it's a platform for your code, which allows it to run without any problems. And it's very simple for developers to use it's API instead of one we have in game. For example, you can retrieve a value from databank, using just one function and without any huge IF/ELSE/PCALL constructions. Same goes for screen / HUD interaction and many other things.

# Key features
  - Modularity. Basically you don't have any functions after OS installing. You have to download some modules, made by SRM developers or other people, and install them too. It may be unusual and NOT as simple as using scripts, created by wrap.lua, but with that logic you will have a full customization freedom. And I'm sure we or other developers will make some kind of core assemblies with modules included. Otherwise, modular structure means you can easily write your own module and integrate it into the system!
  - Simplicity. It's easy to write and integrate your code with SRM. And we are not even talking about usability - our interface is as intuitive and friendly, as possible.
  - Data protection. Everything that comes through the networking subsystem is reliably encoded.
  - API. In-game functions are cool, but they have their weaknesses anyway. Of course, SRM's functions have them too, but they are definately more comfortable.
  - FoF system. Declare your enemies and friends and it will take an effect in many SRM modules.

# When you may want to use SRM
  In almost any case! But there are some situations in which it can be more useful, then in others.
  - Building a battleship. Original chief developer likes PvP and so does the system. It is suitable for combat operations in extreme conditions, because they led to its creation.
  - Building just a big ship. If you have many systems needed to work together, then you will make a nice choice by choosing SRM to organise them all.
  - Well, there will be many modules expanding system's possibilities, so you may know the area of usage even better than we do :).

# Conditions of using
  There aren't any. You can just use, modify and do whatever you want to the system.