module = {}

function module:new()
  local private = {}
    private.SRM_config = {
      name = "myName", -- internal name, uses in HTML. There can be no empty spaces e.g. "My Module" won't work.
      networkName = "hangarsControlServer", -- internal name, needed to use module via emitter.
      displayName = "my awesome module!", -- this name appears on the screen.
      enabled = true, -- Or false if user doesn't want to use module.
      renderOnScreen = true, -- Or false. This parameter defines if module will render on the screen or not.
      useAdditionalScreen = false, -- If module uses additional screen or not
      additionalScreenVar = screen1, -- Variable of additional screen. Only works if module uses it.
      renderOnPlayerScreen = true, -- Same as renderOnScreen, but for player's screen.
      displayInMainWidget = true, -- if you want to display your module separetely from others (on HUD) then set false. Option only works if renderOnPlayerScreen is true.
      widgetCoords = {10, 10}, -- XY coordinates of the custom widget's left top corner. Only works if displayInMainWidget = false.
      widgetSize = {10, 10}, -- XY width and height of the custom widget. Only works if displayInMainWidget = false.
      placeValuesOnHUD = false, -- if you want to use default places 1-12 to display your values. Option only works if renderOnPlayerScreen is true.
      numbersOfHUDValues = {}, -- numbers of places you want to draw your values on: {1, 4, 12}. Don't forget to check that other modules don't use the same places! Option only works if placeValuesOnHUD is true.
      networking = false, -- If module use emitter/receiver or not.
      useStartupHook = false, -- If module will react on system initialization or not.
      useShutdownHook = false -- If module will react on system shutdown or not.
    }


  local public = {}
    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:receiveFromCore(senderNickname, senderModule, message)
      --do whatever you want
    end

    function public:getScreenHTML()
      return 'HTML_code'
    end

    function public:getPlayerScreenHTML()
      return '<div class="' .. private.SRM_config.name .. '" style="position:fixed;left:' .. private.SRM_config.widgetCoords[1] .. 'vw;top:' .. private.SRM_config.widgetCoords[2] .. 'vh;width:' .. private.SRM_config.widgetSize[1] .. 'vw;height:' .. private.SRM_config.widgetSize[2] .. [[vh">
      
      </div>]] -- place everything you want between <div></div>
    end

    function public:onScreenClick(x, y)
      --do something
    end

    function public:onPlayerScreenClick(x, y)
      --do something
    end

    function public:runStartupHook()
      --any code
    end

    function public:runShutdownHook()
      --a few calculations
    end

    function public:getHUDValue(numberOfValue)
      -- determine which value you want to draw on given place

      return 'text' -- for values 1-8 you can return HTML or SVG code also, so you can change the color or type of the text, for example.
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

module_obj = module:new()