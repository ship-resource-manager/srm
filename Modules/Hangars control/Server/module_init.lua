hangars_control_server = {}

function hangars_control_server:new()
  local private = {}
    private.SRM_config = {
      name = "hangarsControlServer", -- internal name, uses in HTML. Have to follow the rules of variables naming.
      networkName = "hangarsControlServer", --internal name, needed to use module via emitter
      displayName = "HANGARS", -- name appears on the screen
      enabled = true, -- Or false if user doesn't want to use module
      renderOnScreen = true, -- Or false. This parameter defines if module will render on the screen or not
      useAdditionalScreen = false, -- If module uses additional screen or not
      renderOnPlayerScreen = false, -- Same as public.renderOnScreen, but for player's screen
      displayInMainWidget = false, -- if you want to display your module separetely from others (on HUD) then set false. Option only works if renderOnPlayerScreen is true.
      placeValuesOnHUD = false, -- if you want to use default places 1-12 to display your values. Option only works if renderOnPlayerScreen is true.
      networking = true, -- If module use emitter/receiver or not
      useStartupHook = true, -- If module will react on system initialization or not
      useShutdownHook = false, -- If module will react on system shutdown or not
      radarVar = radar, --radar's variable
      radarPos = {0, 0, 0}, -- XYZ local position of the radar, divided by 4
      hangars = {}, -- {{hangar_1, false, 10, 20, 10, 20, 10, 50}, {hangar_2, false, 10, 20, 10, 20, 10, 50}} etc. Format: hangar doors switches' variables, their status - free to dock or not (just a cap here, will be changed during the work), local XXYYZZ coordinates (divided by 4)
      max_ship_width = 16, --max width of a ship (in M) that will get a permission to dock.
      max_ship_length = 16, --max length of a ship (in M) that will get a permission to dock.
      max_ship_height = 16, --max height of a ship (in M) that will get a permission to dock.
      ship_length = 128, --your ship's length
      doorsType = '' -- 'door' or 'ff', which means 'force field'
    }

    private.requests = {}
    private.requestsTimestampsAsKeys = {}
    private.requestsIterator = {}
    private.requestsUpdated = false
    private.requestsPage = 1
    private.mode = 'hangars'

    function private:updateRequestsIterator()
      local keys = {}

      for k in pairs(private.requestsTimestampsAsKeys) do keys[#keys + 1] = k end

      table.sort(keys, function(a, b)  
        return a > b
      end)

      private.requestsIterator = keys
    end

    function private:closeHangar(hangarSwitch)
      if private.SRM_config.doorsType == 'door' then
        hangarSwitch.deactivate()
      elseif private.SRM_config.doorsType == 'ff' then
        hangarSwitch.activate()
      end
    end

    function private:openHangar(hangarSwitch)
      if private.SRM_config.doorsType == 'door' then
        hangarSwitch.activate()
      elseif private.SRM_config.doorsType == 'ff' then
        hangarSwitch.deactivate()
      end
    end

    function private:getDoorsStatus(hangarSwitch)
      if private.SRM_config.doorsType == 'door' then
        if hangarSwitch.getState() == 1 then
          return 'opened'
        else
          return 'closed'
        end
      elseif private.SRM_config.doorsType == 'ff' then
        if hangarSwitch.getState() == 0 then
          return 'opened'
        else
          return 'closed'
        end
      end
    end

    function private:findFreeHangar()
      local i = #private.SRM_config.hangars

      for _, hangar in private.SRM_config.hangars do
        if hangar[2] then
          return hangar
        else
          i = i - 1
        end
      end

      if i == 0 then
        return 'NO_HANGARS'
      end
    end

    function private:checkHangarAvailability()
      local radar = private.SRM_config.radarVar

      local entries = radar.getEntries()

      for _, hangar in pairs(private.SRM_config.hangars) do
        hangar[2] = true --it's free to dock; initial status
         
        for i = 1, #hangar[9] do
          hangar[9][i] = 'N/A'
        end
            
        for i = 1, #entries do
          local owner = system.getPlayerName(radar.getConstructOwner(entries[i]))
       	  local size = radar.getConstructSize(entries[i])
		      local width = math.floor(size[1])
          local length = math.floor(size[2])
		      local height = math.floor(size[3])
          local position = radar.getConstructPos(entries[i])            
          local distance = math.floor(math.sqrt(position[1]^2 + position[2]^2 + position[3]^2)) / 1000
                        
          if distance < private.SRM_config.ship_length * 2 then
            if length <= private.SRM_config.max_ship_length and width <= private.SRM_config.max_ship_width and height <= private.SRM_config.max_ship_height then  
              if position[1] > hangar[3] and position[1] < hangar[4] then
                if position[2] > hangar[5] and position[2] < hangar[6] then
                  if position[3] > hangar[7] and position[3] < hangar[8] then
                    hangar[2] = false -- if filled
                    hangar[9] = {owner, entries[i], length}
                    break
                  end
                end
              end
            end
          end
        end
      end
    end

  local public = {}
    function public:receiveFromCore(senderNickname, senderModule, message)
	
      if senderModule == 'hangarsControlClient' then
        message = json.decode(message)
      end

      if message[1] == 'DOCK_REQUEST' and private.requests.message[2] == nil then
        local size = radar.getConstructSize(message[2])

        if size[1] < private.SRM_config.max_ship_width and size[2] < private.SRM_config.max_ship_length and size[3] < private.SRM_config.max_ship_height then
          local hangar = private.findFreeHangar()

          if hangar ~= 'NO_HANGARS' then
            if json.decode(SRM:retrieveFromDatabank('SRM:PERSON_RECORD:' .. senderNickname)).status == 'ally' then
              SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'DOCK_REQUEST:ACCEPTED')
              private.openHangar(hangar[1])
            else
              private.requests[message[2]] = {timestamp = system.getTime(), requestType = 'DOCK_REQUEST'}
              private.requestsUpdated = true
              SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'DOCK_REQUEST:QUEUED')
            end
          else
            SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'DOCK_REQUEST:NO_HANGARS')
          end  
        else
          SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'DOCK_REQUEST:SHIP_TOO_BIG')
        end
      elseif message[1] == 'UNDOCK_REQUEST' and private.requests.message[2] == nil then
        local i = #private.SRM_config.hangars

        for _, hangar in private.SRM_config.hangars do
          if hangar[9][2] == message[2] then
            if json.decode(SRM:retrieveFromDatabank('SRM:PERSON_RECORD:' .. senderNickname)).status == 'ally' then
              SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'UNDOCK_REQUEST:ACCEPTED')
              private.openHangar(hangar[1])
              break
            else
              private.requests[message[2]] = {timestamp = system.getTime(), requestType = 'UNDOCK_REQUEST'}
              private.requestsUpdated = true
              SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'UNDOCK_REQUEST:QUEUED')
              break
            end
          else
            i = i - 1
          end
        end

        if i == 0 then
          SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'UNDOCK_REQUEST:SHIP_NOT_FOUND')
        end
      elseif message[1] == 'CANCEL_UNDOCK_REQUEST' then
        table.remove(private.requests, message[2])
        private.requestsUpdated = true
        SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'UNDOCK_REQUEST:CANCELLED')
      elseif message[1] == 'CANCEL_DOCK_REQUEST' then
        table.remove(private.requests, message[2])
        private.requestsUpdated = true
        SRM:sendToChannel('hangarsControl', hangars_control_server_obj, senderNickname, private.SRM_config.networkName, 'DOCK_REQUEST:CANCELLED')
      end
    end

    function public:getScreenHTML()
      if private.mode == 'hangars' then
        local HTML_code = '<div class="wrap">'
        local i = 1
        
        for hangar_name, hangar in pairs(private.SRM_config.hangars) do
          local class = ''
          local status = private:getDoorsStatus(hangar[1])
              
          if hangar[2] and status == 'opened' then
            class = 'free'
          elseif not hangar[2] and status == 'opened' then
            class = 'filled'
          else
            class = 'restricted'
          end
          
          if i == 3 then
            i = 0
            class = class .. ' last'
          end
      
          i = i + 1

          HTML_code = HTML_code .. '<div class="hangar ' .. class .. '"><div class="info"><div class="info-property">Hangar: ' .. hangar_name .. '</div><div class="info-property">Status: ' .. status .. '</div><div class="info-property">Owner: ' .. string.sub(hangar[9][1], 0, 12) .. '</div><div class="info-property">Ship:  '.. string.sub(private.SRM_config.radarVar.getConstructName(hangar[9][2]), 0, 12) .. '</div><div class="info-property">Length: ' .. string.sub(hangar[9][3], 0, 12) .. ' m</div></div><div class="buttons"><div class="button">OPEN</div><div class="button">CLOSE</div></div></div>'
        end

        return HTML_code .. '</div><div class="mode-switch">REQUESTS</div><style>.mode-switch{position:fixed;right:0;top:50vh;margin-top:-16vh;height:auto;width:4vw;font-size:5vh;line-height:5vh;padding:1vh 0 1vh 0.5vw;writing-mode:tb-rl;transform:rotate(180deg);border:1px solid red;}.wrap{margin-top:4%; margin-left:2%; display: flex; flex-wrap: wrap; align-content: flex-start; width: 80vw; height: 100vh;}.hangar{display: flex; justify-content: space-between; align-items: center; color:#FFFFFF; font-size: 2vh; text-align: center; width: 30%; height: 30%; margin-right: 1%; margin-bottom: 1%;}.last{margin: 0;}.info, .buttons{display: flex; flex-direction: column; justify-content: space-evenly;}.info{align-items: flex-start; margin-left: 2%; width: 63%; height: 100%;}.buttons{width: 35%; height: 100%; font-size: 3vh}.info-property{margin-left: 2%;}.button{height: 5vh; width: 90%; border: 1px solid #FF0000; background-color: #000000; line-height:5vh;}.filled{background-color:#FFA500;}.free{background-color: #008000;}.restricted{background-color:#8B0000;}</style>'
      elseif private.mode == 'requests' then
        
      end
    end

    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:onScreenClick(x, y)
      if private.mode == 'hangars' then
        if x >= 37 and x <= 44 then
          if y >= 11.5 and y <= 16.5 then
            private:openHangar(private.SRM_config.hangars[1][1])
          end

          if y >= 23 and y <= 28 then
            private:closeHangar(private.SRM_config.hangars[1][1])
          end

          if y >= 42.8 and y <= 47.8 then
            private:openHangar(private.SRM_config.hangars[4][1])
          end

          if y >= 54.5 and y <= 59.5 then
            private:closeHangar(private.SRM_config.hangars[4][1])
          end

          if y >= 74 and y <= 79 then
            private:openHangar(private.SRM_config.hangars[7][1])
          end

          if y >= 85.5 and y <= 90.5 then
            private:closeHangar(private.SRM_config.hangars[7][1])
          end
        end

        if x >= 61.8 and x <= 68.8 then
          if y >= 11.5 and y <= 16.5 then
            private:openHangar(private.SRM_config.hangars[2][1])
          end

          if y >= 23 and y <= 28 then
            private:closeHangar(private.SRM_config.hangars[2][1])
          end

          if y >= 42.8 and y <= 47.8 then
            private:openHangar(private.SRM_config.hangars[5][1])
          end

          if y >= 54.5 and y <= 59.5 then
            private:closeHangar(private.SRM_config.hangars[5][1])
          end

          if y >= 74 and y <= 79 then
            private:openHangar(private.SRM_config.hangars[8][1])
          end

          if y >= 85.5 and y <= 90.5 then
            private:closeHangar(private.SRM_config.hangars[8][1])
          end
        end

        if x >= 86.6 and x <= 93.6 then
          if y >= 11.5 and y <= 16.5 then
            private:openHangar(private.SRM_config.hangars[3][1])
          end

          if y >= 23 and y <= 28 then
            private:closeHangar(private.SRM_config.hangars[3][1])
          end

          if y >= 42.8 and y <= 47.8 then
            private:openHangar(private.SRM_config.hangars[6][1])
          end

          if y >= 54.5 and y <= 59.5 then
            private:closeHangar(private.SRM_config.hangars[6][1])
          end

          if y >= 74 and y <= 79 then
            private:openHangar(private.SRM_config.hangars[9][1])
          end

          if y >= 85.5 and y <= 90.5 then
            private:closeHangar(private.SRM_config.hangars[9][1])
          end
        end

      elseif private.mode == 'requests' then
        if false then
          table.remove(private.requests, private.requests[private.requestsTimestampsAsKeys[private.requestsIterator[(private.requestsPage - 1) * 8 + 1]]])
          private.requestsUpdated = true
          SRM:sendToChannel('hangarsControl', hangars_control_server_obj, system.getPlayerName(private.SRM_config.radarVar.getConstructOwner(id)), private.SRM_config.networkName, 'DOCK_REQUEST:DENIED')
        end
  
        if false then
          SRM:sendToChannel('hangarsControl', hangars_control_server_obj, system.getPlayerName(private.SRM_config.radarVar.getConstructOwner(id)), private.SRM_config.networkName, 'DOCK_REQUEST:ACCEPTED')
        end
  
        if false then
          SRM:sendToChannel('hangarsControl', hangars_control_server_obj, system.getPlayerName(private.SRM_config.radarVar.getConstructOwner(id)), private.SRM_config.networkName, 'UNDOCK_REQUEST:DENIED')
        end
  
        if false then
          SRM:sendToChannel('hangarsControl', hangars_control_server_obj, system.getPlayerName(private.SRM_config.radarVar.getConstructOwner(id)), private.SRM_config.networkName, 'UNDOCK_REQUEST:ACCEPTED')
        end
      end

      SRM:updateScreen(hangars_control_server_obj)
    end

    function public:updateStatus()
      private:checkHangarAvailability()

      for id, request in pairs(private.requests) do
        if system.getTime() - request.timestamp >= 30 then
          table.remove(private.requests, id)

          SRM:sendToChannel('hangarsControl', hangars_control_server_obj, system.getPlayerName(private.SRM_config.radarVar.getConstructOwner(id)), private.SRM_config.networkName, 'DOCK_REQUEST:EXPIRED')
        end
      end

      if private.requestsUpdated then
        private.requestsTimestampsAsKeys = {}

        for id, request in pairs(private.requests) do
          private.requestsTimestampsAsKeys[request.timestamp] = id
        end

        private:updateRequestsIterator()
        private.requestsUpdated = false
      end
    end
  
    function public:runStartupHook()
      for _, hangar in pairs(private.SRM_config.hangars) do
        hangar[3] = hangar[3] - private.SRM_config.radarPos[1]
        hangar[4] = hangar[4] - private.SRM_config.radarPos[1]
        hangar[5] = hangar[5] - private.SRM_config.radarPos[2]
        hangar[6] = hangar[6] - private.SRM_config.radarPos[2]
        hangar[7] = hangar[7] - private.SRM_config.radarPos[3]
        hangar[8] = hangar[8] - private.SRM_config.radarPos[3]
        hangar[9] = {'N/A', 'N/A', 'N/A'}
      end
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

hangars_control_server_obj = hangars_control_server:new()