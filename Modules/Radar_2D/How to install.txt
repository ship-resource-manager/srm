1. Connect a radar to the programming board. In the configuration file of the module, place the name of the radar's variable into field "radarVar". Radar must "watch" right forward and up or horrible things will happen.
2. Place a code from "configuration.lua" to unit.start(), before SRMCore declaration.
3. Place a code from "module_init.lua" to unit.start(), after configuration, but before SRMCore declaration.
4. Place a code from "updater.lua" to any method which you will call to update entries' list. Usually it's system.update() or system.actionStart(option1-9). You can also make this module work automatically but only when necessary by writing something like that:

system.update()
	if radarListStatus then
		SRM:updateScreen(radar_list_config.object)
	end
	
system.actionStart(option 1)
	radarListStatus = true

system.actionStart(option 2)
	radarListStatus = false
	
Enjoy your Radar's Entries List module!