radar_marks = {}

function radar_marks:new()
  local private = {}
    private.name = radar_marks_config.name
    private.displayName = radar_marks_config.displayName


  local public = {}
    function public:receiveFromCore(senderModule, message)
      --no integrations or networking functions yet
    end

    function public:getScreenHTML()
      --not renderable on the screen
    end

    function public:getPlayerScreenHTML()
      local HTML_code = '<div class="example_div"></div><style>.example_div{width:40px;height:40px;left:50%;top:50%;background-color:red;}</style>'

      return HTML_code
    end

    function public:getModuleName()
      return private.name
    end

    function public:getModuleDisplayName()
      return private.displayName
    end

    function public:onScreenClick()
      --not clickable
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

if radar_marks_config.enabled == true then
  radar_marks_obj = radar_marks:new()
  radar_marks_config.object = radar_marks_obj
end