fuel = {}

function fuel:new()
  local private = {}
    private.SRM_config = {
        name = "fuelmeter",
        displayName = "FUEL",
        enabled = true,
        renderOnScreen = true,
        useAdditionalScreen = false,
        renderOnPlayerScreen = true,
        displayInMainWidget = true,
        placeValuesOnHUD = false,
        networking = false,
        useStartupHook = true,
        useShutdownHook = false
    }   
    private.sfuelmax = {s = 720, m = 2880, l = 23040} -- put here maximum fuel level according to skills applied
    private.afuelmax = {xs = 180, s = 720, m = 2880, l = 23040} -- put here maximum fuel level according to skills applied
    private.rfuelmax = {xs = 560, s = 1120, m = 8960, l = 70000} -- put here maximum fuel level according to skills applied
    private.stankmass = {s = 182.67, m = 988.67, l = 5481.27}
    private.atankmass = {xs = 35.03, s = 182.67, m = 988.67, l = 5481.27}
    private.rtankmass = {xs = 173.42, s = 886.72, m = 4724.43, l = 25740}
    private.stanks = {__len__ = 0, tanks = {}}
    private.atanks = {__len__ = 0, tanks = {}}
    private.rtanks = {__len__ = 0, tanks = {}}
    private.switch = {}
    private.switch_state = 'space'
    private.widget_switch = {}
    private.widget_switch_state = 'space'
    private.levels = 20
    private.coreVar = core
    
    function private:switchMode(switch1, source)
      if source == 'screen' then
        if switch1 == 'space' then
            private.switch_state = 'space'
            private.switch = private.stanks
        elseif switch1 == 'atmo' then
            private.switch_state = 'atmo'
            private.switch = private.atanks
        elseif switch1 == 'rocket' then
            private.switch_state = 'rocket'         
            private.switch = private.rtanks
        end
      elseif source == 'widget' then
        if switch1 == 'space' then
            private.widget_switch_state = 'space'
            private.widget_switch = private.stanks
        elseif switch1 == 'atmo' then
            private.widget_switch_state = 'atmo'
            private.widget_switch = private.atanks
        elseif switch1 == 'rocket' then
            private.widget_switch_state = 'rocket'         
            private.widget_switch = private.rtanks
        end
      end

      SRM:updateScreen(fuel_obj)
    end
    
    function private:getFuelData(source)
        local HTML_code = ''
        local psw = {}
        local pswst = ''

        if source == 'screen' then
            psw = private.switch.tanks
            pswst = private.switch_state
        elseif source == 'widget' then
            psw = private.widget_switch.tanks
            pswst = private.widget_switch_state
        end
        
        if psw == nil then
            psw = {}
        end    
        
        for _, value in pairs(psw) do  
            
            HTML_code = HTML_code .. '<div class="tank"><div class="values">'..value.name..'<br>' .. value.fuel .. ' L</div><div class="levels-wrap">'

            local currentLevel = 0

            if pswst == 'space' then
                currentLevel = math.ceil(value.fuel / (private.sfuelmax[value.size] / private.levels))
            elseif pswst == 'atmo' then
                currentLevel = math.ceil(value.fuel / (private.afuelmax[value.size] / private.levels))
            elseif pswst == 'rocket' then
                currentLevel = math.ceil(value.fuel / (private.rfuelmax[value.size] / private.levels))
            end

            for i = 1, private.levels do
                if i > currentLevel then
                    HTML_code = HTML_code..'<div class="level empty"></div>'
                else
                    HTML_code = HTML_code..'<div class="level full"></div>'
                end
            end

            HTML_code = HTML_code..'</div></div>'
        end

        return HTML_code
    end
  
  local public = {}
    function public:receiveFromCore(senderModule, message)
      --no actions to do here
    end

    function public:getScreenHTML()          
        local HTML_code = '<div class="wrap"><div class="head">'
            
        if private.switch_state == 'space' then
            HTML_code = HTML_code .. '<div class="button full">SPACE FUEL</div><div class="button">ATMO FUEL</div><div class="button">ROCKET FUEL</div>'
        elseif private.switch_state == 'atmo' then    
            HTML_code = HTML_code .. '<div class="button">SPACE FUEL</div><div class="button full">ATMO FUEL</div><div class="button">ROCKET FUEL</div>'
        elseif private.switch_state == 'rocket' then
            HTML_code = HTML_code .. '<div class="button">SPACE FUEL</div><div class="button">ATMO FUEL</div><div class="button full">ROCKET FUEL</div>'
        end
        
        HTML_code = HTML_code .. '</div><div class="tanks">' .. private:getFuelData('screen') .. '</div>'
		
        if private.switch['__len__'] ~= nil then
        	local column = math.floor(75 / private.switch['__len__'])

        	HTML_code = HTML_code .. '</div><style>.wrap{display:flex;flex-direction:column;width:75vw;height:100vh;margin-left:2.5vw;}.tanks{display:flex;flex-direction:row;width:75vw;}.tank{display:flex;flex-direction:column-reverse;margin-right:1vw;}.levels-wrap{display:flex;flex-direction:column-reverse;}.level{width:' .. column .. 'vw;height:4vh;border:1px solid white;}.full{background-color:green;}.empty{background-color:red;}.values{font-size:2vw;text-align:right;width:' .. column .. ';}.head{display:flex;flex-direction:row;width:75vw;height:10vh;margin-bottom:2vh;}.button{font-size:3vw;border:1px solid white;width:25vw;height:10vh;text-align:center;}</style>'
		end
        
        return HTML_code
	end

    function public:getPlayerScreenHTML()
        local HTML_code = '<div class="wrap"><div class="head">'
            
        if private.widget_switch_state == 'space' then
            HTML_code = HTML_code .. '<div class="button full">SPACE FUEL</div><div class="button">ATMO FUEL</div><div class="button">ROCKET FUEL</div>'
        elseif private.widget_switch_state == 'atmo' then    
            HTML_code = HTML_code .. '<div class="button">SPACE FUEL</div><div class="button full">ATMO FUEL</div><div class="button">ROCKET FUEL</div>'
        elseif private.widget_switch_state == 'rocket' then
            HTML_code = HTML_code .. '<div class="button">SPACE FUEL</div><div class="button">ATMO FUEL</div><div class="button full">ROCKET FUEL</div>'
        end
        
        HTML_code = HTML_code .. '</div><div class="tanks">' .. private:getFuelData('widget') .. '</div>'
		
        if private.widget_switch['__len__'] ~= nil then
        	local row = math.floor(19 / private.widget_switch['__len__'])

        	HTML_code = HTML_code .. '</div><style>.wrap{display:flex;flex-direction:column;width:25vw;height:22.5vh;}.tanks{display:flex;flex-direction:column;width:25vw;}.tank{display:flex;justify-content:space-around;margin-bottom:0.2vh;}.levels-wrap{display:flex;}.level{width:0.9vw;height:' .. row .. ';border:1px solid white;max-height:3.8vh;}.full{background-color:green;}.empty{background-color:red;}.values{text-align:right;font-size:0.7vw;width:6vw;}.head{display:flex;flex-direction:row;width:25vw;height:2.5vh;margin-top:0.5vh;margin-bottom:0.5vh;}.button{font-size:3vw;border:1px solid white;width:8.37vw;height:2.5vh;text-align:center;}</style>'
        end
        
        return HTML_code
    end

    function public:getModuleName()
      return private.SRM_config.name
    end

    function public:getModuleDisplayName()
      return private.SRM_config.displayName
    end

    function public:onScreenClick(x, y)
        if y > 0 and y <= 20 then
            if x > 22.5 and x <= 47.5 then
                private:switchMode('space', 'screen')
            elseif x > 47.5 and x <= 72.5 then
                private:switchMode('atmo', 'screen')
            elseif x > 72.5 and x <= 97.5 then
                private:switchMode('rocket', 'screen')
            end
        end
    end
    
    function public:onPlayerScreenClick(x, y)
        if y >= 76.6 and y <= 79 then
            if x >= 74 and x <= 82.3 then
                private:switchMode('space', 'widget')
            elseif x >= 82.4 and x <= 90.7 then
                private:switchMode('atmo', 'widget')
            elseif x >= 90.8 and x <= 99 then
                private:switchMode('rocket', 'widget')
            end
        end
    end

    function public:runStartupHook()
        for _, value in pairs(private.coreVar.getElementIdList()) do
            local _type = private.coreVar.getElementTypeById(value)
            local _name = ''
            local _size = ''
		  
            
            if _type == 'Space Fuel Tank' or _type == 'Atmospheric Fuel Tank' or _type == 'Rocket Fuel Tank' then
                _name = string.upper(private.coreVar.getElementNameById(value))

                if string.find(_name, 'SIZEXS') ~= nil then
                    _size = 'xs'
                    _name = string.gsub(string.gsub(_name, 'SIZEXS', ''), '  ', ' ')
                elseif string.find(_name, 'SIZES') ~= nil then
                    _size = 's'
                    _name = string.gsub(string.gsub(_name, 'SIZES', ''), '  ', ' ')
                elseif string.find(_name, 'SIZEM') ~= nil then
                    _size = 'm'
                    _name = string.gsub(string.gsub(_name, 'SIZEM', ''), '  ', ' ')
                elseif string.find(_name, 'SIZEL') ~= nil then
                    _size = 'l'
                    _name = string.gsub(string.gsub(_name, 'SIZEL', ''), '  ', ' ')
                else
                    local status = true
                    
                    for _, value in pairs(SRM:getNotificationsList()) do
                        if value.heading == private.SRM_config.displayName then
                            status = false
                            break
                        end
                    end
                    
                    if status then
                        SRM:makeNotification(private.SRM_config.displayName, '<span style="color:red;">ERROR!</span><br>Unable to determine fuel tank size. The module will not work and may interrupt other processes.')    
                    end
                end
            end

            if _type == 'Space Fuel Tank' then
                private.stanks.tanks[value] = {size = _size, name = _name, fuel = 0}
                private.stanks.__len__ = private.stanks.__len__ + 1
            elseif _type == 'Atmospheric Fuel Tank' then
                private.atanks.tanks[value] = {size = _size, name = _name, fuel = 0}
                private.atanks.__len__ = private.atanks.__len__ + 1
            elseif _type == 'Rocket Fuel Tank' then
                private.rtanks.tanks[value] = {size = _size, name = _name, fuel = 0}
                private.rtanks.__len__ = private.rtanks.__len__ + 1
            end
        end
        
        private.switch = private.stanks
        
        private.widget_switch = private.stanks
    end

    function public:updateInfo()
        for key, value in pairs(private.stanks.tanks) do
            value.fuel = math.floor((private.coreVar.getElementMass(key) - private.stankmass[value.size]) / 6)
        end

        for key, value in pairs(private.atanks.tanks) do
            value.fuel = math.floor((private.coreVar.getElementMass(key) - private.atankmass[value.size]) / 4)
        end

        for key, value in pairs(private.rtanks.tanks) do
            value.fuel = math.floor((private.coreVar.getElementMass(key) - private.rtankmass[value.size]) / 0.8)
        end
    end
    
    function public:getSRMConfiguration()
        return private.SRM_config
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

fuel_obj = fuel:new()