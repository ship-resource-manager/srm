module = {}

function module:new()
  local private = {}
    private.name = module_config.name
    private.displayName = module_config.displayName
	private.networkName = module_config.networkName


  local public = {}
    function public:receiveFromCore(senderModule, message)
      --do whatever you want
    end

    function public:getModuleName()
      return private.name
    end

    function public:getModuleDisplayName()
      return private.displayName
    end
	
	function public:getModuleNetworkName()
      return private.networkName
    end


  setmetatable(public, self)
  self.__index = self 
  return public
end

if module_config.enabled == true then
  module_obj = module:new()
  module_config.object = module_obj
end