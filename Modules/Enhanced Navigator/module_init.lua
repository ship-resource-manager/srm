enhancedNavigator = {}

function enhancedNavigator:new()
  local private = {}
    private.SRM_config = {
      name = "enhancedNavigator",
      networkName = "enhancedNavigator",
      displayName = "NAV",
      enabled = true,
      renderOnScreen = false,
      useAdditionalScreen = false,
      renderOnPlayerScreen = true,
      displayInMainWidget = false,
      placeValuesOnHUD = true,
      numbersOfHUDValues = {6, 7},
      networking = false,
      useStartupHook = true,
      useShutdownHook = false
    }

    private.ab_status = 0
    private.ab_timestamp = 0
    private.an_status = 0
    private.switchOptionKey = 2134254124312


  local public = {}
    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:receiveFromCore(senderNickname, senderModule, message) end

    function public:getPlayerScreenHTML() return '' end

    function public:onPlayerScreenClick(x, y) end

    function public:runStartupHook()
      system.print('Enhanced Navigator: Autobrake ready.')
      system.print('Enhanced Navigator: RCS ready.')
    end

    function public:getHUDValue(numberOfValue)
      local status = {}

      if numberOfValue == 6 then
        if private.an_status == 0 then
          status = {'RCS', 'red'}
        else
          status = {'RCS', '#32CD32'}
        end
      end

      if numberOfValue == 7 then
        if private.ab_status == 0 then
          status = {'autobrake', 'red'}
        else
          status = {'autobrake', '#32CD32'}
        end
      end

      return '<span style="color:' .. status[2] .. '">' .. status[1] .. '</span>'
    end

    function public:switchNavigatorOption(key, option)
      if key == private.switchOptionKey then
        if option == 'ab' then
          local ab_tsp = system.getTime()

          if ab_tsp - private.ab_timestamp <= 0.2 and private.ab_timestamp ~= 0 then
            private.ab_status = (private.ab_status - 1) * -1
            private.ab_timestamp = 0
            if private.ab_status == 1 then
              brakeInput = 1
              system.print('Enhanced Navigator: Autobrake engaged')
            else
              brakeInput = 0
              system.print('Enhanced Navigator: Autobrake disengaged')
            end
          else
            private.ab_timestamp = ab_tsp
          end
          
        elseif option == 'an' then
          private.an_status = (private.an_status - 1) * -1

          if private.an_status == 0 then
            system.print('Enhanced Navigator: RCS disengaged')
          else
            system.print('Enhanced Navigator: RCS engaged')
          end
        end
      end
    end

    function public:getAbStatus() return private.ab_status end
    function public:getAnStatus() return private.an_status end

  setmetatable(public, self)
  self.__index = self 
  return public
end

enhancedNavigator_obj = enhancedNavigator:new()