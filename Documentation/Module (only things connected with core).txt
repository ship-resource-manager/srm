# Basics
  When the system starts, core is looking for module configurations, and if it find active one, it will create the object of a module's main class, filling it with the values found in configuration file. Then you can do whatever you want with a module's object.


# Main class methods and properties
  private.name - internal system name of the module.
  private.displayName - name that appear on screen (taken from configuration file)
  
  public:receiveFromCore(senderModule, message) - event handler, triggers when you get a message from the core. You can process it in any way and this is how modules communicate with each other.
  public:getScreenHTML() - core calls it when it needs to render the page on the screen with current state of all elements (usually you call SRM:updateScreen() to make it do that).
  public:getPlayerScreenHTML() - same as public:getScreenHTML, but for player's screen (HUD).
  public:getModuleName() - returns private.name
  public:getModuleDisplayName() - returns private.displayName
  public:onScreenClick(x, y) - reacts when a player clicks on the screen's work area and the module's tab is active.