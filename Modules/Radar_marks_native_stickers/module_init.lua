radar_marks = {}

function radar_marks:new()
  local private = {}
    private.name = radar_marks_config.name
    private.displayName = radar_marks_config.displayName
    private.targetsList = {}


  local public = {}
    function public:receiveFromCore(senderModule, message)
      --no integrations yet
    end

    function public:getScreenHTML()
     --doesn't appear on the screen
    end

    function public:updateMarks()
      local radar = radar_marks_config.radarVar
	  local core = radar_marks_config.coreVar
      
      for radarID, stickerID in pairs(private.targetsList) do
        local position = radar.getConstructPos(radarID)

        core.moveSticker(stickerID, position[1], position[2], position[3])
      end
    end

    function public:addMark(id)
      local radar = radar_marks_config.radarVar
	  local core = radar_marks_config.coreVar
	  
      local position = radar.getConstructPos(id)

      local stickerID = core.spawnNumberSticker(0, position[1], position[2], position[3], 'front')

      private.targetsList[id] = stickerID
    end

    function public:removeMark(id)
      local radar = radar_marks_config.radarVar
	  local core = radar_marks_config.coreVar

      local stickerID = private.targetsList[id]

      core.deleteSticker(stickerID)
    end

    function public:getPlayerScreenHTML()
      --using core's API instead of system's API
    end

    function public:getModuleName()
      return private.name
    end

    function public:getModuleDisplayName()
      return private.displayName
    end

    function public:onScreenClick()
      --not clickable
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

if radar_marks_config.enabled == true then
  radar_marks_obj = radar_marks:new()
  radar_marks_config.object = radar_marks_obj
end

local entries = radar.getEntries()

for i = 1, #entries do
  radar_marks_obj:addMark(entries[i])
end

return radar_marks_obj