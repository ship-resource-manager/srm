private_rooms_server = {}

function private_rooms_server:new()
  local private = {}
    private.name = private_rooms_server_config.name
    private.displayName = private_rooms_server_config.displayName
	private.networkName = private_rooms_server_config.networkName
	private.screenVar = private_rooms_server_config.screenVar


  local public = {}
    function public:receiveFromCore(senderModule, message)
      --do whatever you want
    end

    function public:getModuleName()
      return private.name
    end

    function public:getModuleDisplayName()
      return private.displayName
    end
	
	function public:getModuleNetworkName()
      return private.networkName
    end
	
	function public:updateState()
	  local dbKeys = json.decode(databank.getKeys())
	  local timestamp = system.getTime()
	  
	  local HTML_code = '<div class="container"><div class="row">'
	  local i = 1
	  local j = 1
	  
	  
	  for key in pairs(dbKeys) do
	    if string.find(key, 'PR_RECORD') ~= nil then
		  local value = SRM:retrieveFromDatabank(key)
		  
		  if value == 'TIME_EXCEED' or value == 'SRM:NO_KEY' then
		    HTML_code = HTML_code .. '<div class="room free">' .. string.sub(dbKeys[k], 11) .. '</div>'
		  elseif timestamp - json.decode(value)[2] >= 86400 then
		    HTML_code = HTML_code .. '<div class="room free">' .. string.sub(dbKeys[k], 11) .. '</div>'
			
		    SRM:sendToDatabank(dbKeys[k], 'TIME_EXCEED')
          elseif timestamp - json.decode(value)[2] <= 86400 then
            HTML_code = HTML_code .. '<div class="room locked">' .. string.sub(dbKeys[k], 11) .. '</div>'
		  end
		  
		  if i == 10 then
		    if j == 100 then
		      break
		    end
			
		    i = 1
		    HTML_code = HTML_code + '</div><div class="row">'
		  else
		    i = i + 1
		  end
		  
		  j = j + 1
		end
	  end
	  
	  HTML_code = HTML_code + '</div></div><style>*{margin:0;padding:0;background-color:#000000;}.container{width:100vw;height:100vh;display:flex;flex-direction:column;align-items:center;}.row{width:100vw;height:10vh;display:flex;align-items:center;}.room{width:8vw;height:8vh;margin:1vh 1vw 1vh 1vw;text-align:center;line-height:8vh;color:#FFFFFF;font-size:4vh;}.free{background-color:#008000;}.locked{background-color:#8B0000;}</style>'
	  
	  private.screen.setHTML(HTML_code)
	end

  
  setmetatable(public, self)
  self.__index = self 
  return public
end

if private_rooms_server_config.enabled == true then
  private_rooms_server_obj = private_rooms_server:new()
  private_rooms_server_config.object = private_rooms_server_obj
end