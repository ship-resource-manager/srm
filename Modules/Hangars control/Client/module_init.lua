hangars_control_client = {}

function hangars_control_client:new()
  local private = {}
    private.SRM_config = {
      name = "hangarsControlClient", -- internal name, uses in HTML. There can be no empty spaces e.g. "My Module" won't work.
      networkName = "hangarsControlClient", -- internal name, needed to use module via emitter.
      displayName = "DOCKS", -- this name appears on the screen.
      enabled = true, -- Or false if user doesn't want to use module.
      renderOnScreen = false, -- Or false. This parameter defines if module will render on the screen or not.
      useAdditionalScreen = false, -- If module uses additional screen or not
      renderOnPlayerScreen = true, -- Same as renderOnScreen, but for player's screen.
      displayInMainWidget = true, -- if you want to display your module separetely from others (on HUD) then set false. Option only works if renderOnPlayerScreen is true.
      placeValuesOnHUD = false, -- if you want to use default places 1-12 to display your values. Option only works if renderOnPlayerScreen is true.
      networking = true, -- If module use emitter/receiver or not.
      useStartupHook = false, -- If module will react on system initialization or not.
      useShutdownHook = false -- If module will react on system shutdown or not.
    }

    private.radarVar = radar
    private.coreVar = core


  local public = {}
    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:receiveFromCore(senderNickname, senderModule, message)
      --do whatever you want
    end

    function public:getPlayerScreenHTML()
      return 'HTML_code'
    end

    function public:onScreenClick(x, y)
      --do something
    end

    function public:onPlayerScreenClick(x, y)
      --do something
    end

    function public:runStartupHook()
      --any code
    end

    function public:runShutdownHook()
      --a few calculations
    end

    function public:getHUDValue(numberOfValue)
      -- determine which value you want to draw on given place

      return 'text' -- for values 1-8 you can return HTML or SVG code also, so you can change the color or type of the text, for example.
    end

  setmetatable(public, self)
  self.__index = self 
  return public
end

hangars_control_client_obj = hangars_control_client:new()