module = {}

function module:new()
  local private = {}
    private.SRM_config = {
      name = "myName",
      networkName = "hangarsControlServer",
      displayName = "my awesome module!",
      enabled = true,
      renderOnScreen = false,
      renderOnPlayerScreen = true,
      displayInMainWidget = false,
      widgetCoords = {80, 45},
      widgetSize = {10, 5},
      placeValuesOnHUD = false,
      networking = false,
      useStartupHook = false,
      useShutdownHook = false
    }


  local public = {}
    function public:getSRMConfiguration()
      return private.SRM_config
    end

    function public:receiveFromCore(senderNickname, senderModule, message) end

    function public:getPlayerScreenHTML()
      return '<div class="' .. private.SRM_config.name .. '" style="position:fixed;left:' .. private.SRM_config.widgetCoords[1] .. ' vw;top:' .. private.SRM_config.widgetCoords[2] .. ' vh;width:' .. private.SRM_config.widgetSize[1] .. ' vw;height:' .. private.SRM_config.widgetSize[2] .. [[ vh">
      
      </div>]] -- place everything you want between <div></div>
    end

    function public:onPlayerScreenClick(x, y) end
    

  setmetatable(public, self)
  self.__index = self 
  return public
end

module_obj = module:new()